package com.kfwebstandard.basicservlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Servlet implementation class BasicServlet02
 */
@WebServlet(description = "Example of a working servlet", urlPatterns = {"/BasicServlet02"})
public class BasicServlet02 extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private final static Logger LOG = LoggerFactory.getLogger(BasicServlet02.class);

    /**
     * Servlet that just says Hello
     *
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String servletName = getServletConfig().getServletName();

        response.setContentType("text/html");

        try (PrintWriter writer = response.getWriter()) {
            writer.print("<html><head><link rel='stylesheet' href='/BasicServlets01/styles/main.css' type='text/css'/> </head>"
                    + "<body><h1>Hello from " + servletName
                    + "</h2></body></html>");
        }
    }

}
