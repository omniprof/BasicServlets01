package com.kfwebstandard.basicservlets;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author omni__000
 */
@WebServlet(name = "ServletConfigDemoServlet",
        urlPatterns = {"/BasicServlet04"},
        initParams = {
            @WebInitParam(name = "admin", value = "Bullwinkle J. Moose"),
            @WebInitParam(name = "email", value = "rocky@squirrel.com")
        }
)
public class BasicServlet04 extends HttpServlet {

    private final static Logger LOG = LoggerFactory.getLogger(BasicServlet04.class);

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        ServletConfig servletConfig = getServletConfig();
        String admin = servletConfig.getInitParameter("admin");
        String email = servletConfig.getInitParameter("email");

        response.setContentType("text/html");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet BasicServlet04</title>");
            out.println("<link rel='stylesheet' href='/BasicServlets01/styles/main.css' type='text/css'/>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet BasicServlet04 at " + request.getContextPath() + "</h1>");
            out.print("Admin:" + admin + "<br/>Email:" + email);
            out.println("</body>");
            out.println("</html>");
        }
    }
}
