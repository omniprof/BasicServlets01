package com.kfwebstandard.basicservlets;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Ken
 */
@WebServlet(urlPatterns = {"/FirstServlet"})
public class BasicServlet05a extends HttpServlet {

    private final static Logger LOG = LoggerFactory.getLogger(BasicServlet05a.class);

    private static final String TITLE = "Demo of Forward and Redirect 01";

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            // Now for some ugly code
            out.println("<html>");
            out.println("<head>");
            out.println("<title>" + TITLE + "</title>");
            out.println("<link rel='stylesheet' href='/BasicServlets01/styles/main.css' type='text/css'/>");
            out.println("</head>");
            out.println("<body><h1>" + TITLE + "</h1>");

            // There is not form action so the submit will call its source
            // that is this servlet
            out.println("<form method='post'>");

            out.println("Name:");
            out.println("<input name='name'/></br>");

            out.println("<h1>Press the button to be forwarded to another page</h1></br>");

            out.println("<input type='submit' value='Submit'/>");
            out.println("</form>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        // Retrieve parameters from the query string
        String theName = request.getParameter("name");
        // We can imagine more significant work being carried out
        // before we store a result in the request object
        request.setAttribute("name", theName);

        // Now a forward
//        String url = "/SecondServlet";
//        getServletContext().getRequestDispatcher(url)
//                .forward(request, response);
        // Now a redirect
        String url = "http://localhost:8080/BasicServlets01/SecondServlet";
//        String url = "/BasicServlets01/SecondServlet";
        response.sendRedirect(url);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "BasicServlet05a";
    }

}
