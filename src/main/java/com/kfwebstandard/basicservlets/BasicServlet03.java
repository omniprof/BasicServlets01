package com.kfwebstandard.basicservlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Servlet implementation class BasicServlet03
 */
@WebServlet(description = "Example of a form", urlPatterns = {"/BasicServlet03"})
public class BasicServlet03 extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private final static Logger LOG = LoggerFactory.getLogger(BasicServlet03.class);
    private static final String TITLE = "Order Form";

    /**
     * URLs entered in the address bar or taken from a bookmark are always GET
     * requests
     *
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

        // Define the type of content that will appear in the http response
        response.setContentType("text/html");

        try (
                // The PrintWriter object allows you to write/transmit HTML to the
                // browser that issued the request.
                PrintWriter out = response.getWriter()) {
            // Now for some ugly code
            out.println("<html>");
            out.println("<head>");
            out.println("<meta charset='utf-8'>");
            out.println("<title>" + TITLE + "</title>");
            out.println("<link rel='stylesheet' href='/BasicServlets01/styles/main.css' type='text/css'/>");
            out.println("</head>");
            out.println("<body><h1>" + TITLE + "</h1>");

            // This form will call the post method
            out.println("<form method='delete'>");
            out.println("<table>");
            out.println("<tr>");
            out.println("<td>Name:</td>");
            out.println("<td><input name='name'/></td>");
            out.println("</tr>");
            out.println("<tr>");
            out.println("<td>Address:</td>");
            out.println("<td><textarea name='address' "
                    + "cols='40' rows='5'></textarea></td>");
            out.println("</tr>");
            out.println("<tr>");
            out.println("<td>Country:</td>");
            out.println("<td><select name='country'>");
            out.println("<option>Canada</option>");
            out.println("<option>United States</option>");
            out.println("</select></td>");
            out.println("</tr>");
            out.println("<tr>");
            out.println("<td>Delivery Method:</td>");
            out.println("<td><input type='radio' " + "name='deliveryMethod'"
                    + " value='First Class'/>First Class");
            out.println("<input type='radio' " + "name='deliveryMethod' "
                    + "value='Second Class'/>Second Class</td>");
            out.println("</tr>");
            out.println("<tr>");
            out.println("<td>Shipping Instructions:</td>");
            out.println("<td><textarea name='instruction' "
                    + "cols='40' rows='5'></textarea></td>");
            out.println("</tr>");
            out.println("<tr>");
            out.println("<td>&nbsp;</td>");
            out.println("<td><textarea name='instruction' "
                    + "cols='40' rows='5'></textarea></td>");
            out.println("</tr>");
            out.println("<tr>");
            out.println("<td>Please send me the latest " + "product catalog:</td>");
            out.println("<td><input type='checkbox' "
                    + "name='catalogRequest'/></td>");
            out.println("</tr>");
            out.println("<tr>");
            out.println("<td>&nbsp;</td>");
            out.println("<td><input type='reset'/>" + "<input type='submit'/></td>");
            out.println("</tr>");
            out.println("</table>");
            out.println("</form>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    /**
     * For the purpose of this example the form in the GET method uses POST for
     * the submit
     *
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    @Override
    protected void doDelete(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

        // Define the type of content that will appear in the http response
        response.setContentType("text/html");

        // Retrieve parameters from the query string
        String theName = request.getParameter("name");
        String theAddress = request.getParameter("address");
        String theCountry = request.getParameter("country");
        String[] instructions = request.getParameterValues("instruction");
        String theDeliveryMethod = request.getParameter("deliveryMethod");
        String theCatalogRequest = request.getParameter("catalogRequest");

        try (
                // The PrintWriter object allows you to write/transmit HTML to the
                // browser that issued the request.
                PrintWriter out = response.getWriter()) {
            // more ugly code
            out.println("<html>");
            out.println("<head>");
            out.println("<title>" + TITLE + "</title></head>");
            out.println("</head>");
            out.println("<body><h1>" + TITLE + "</h1>");
            out.println("<table>");
            out.println("<tr>");
            out.println("<td>Name:</td>");
            out.println("<td>" + theName + "</td>");
            out.println("</tr>");
            out.println("<tr>");
            out.println("<td>Address:</td>");
            out.println("<td>" + theAddress + "</td>");
            out.println("</tr>");
            out.println("<tr>");
            out.println("<td>Country:</td>");
            out.println("<td>" + theCountry + "</td>");
            out.println("</tr>");
            out.println("<tr>");
            out.println("<td>Shipping Instructions:</td>");
            out.println("<td>");
            if (instructions != null) {
                for (String instruction : instructions) {
                    out.println(instruction + "<br/>");
                }
            }
            out.println("</td>");
            out.println("</tr>");
            out.println("<tr>");
            out.println("<td>Delivery Method:</td>");
            out.println("<td>" + theDeliveryMethod + "</td>");
            out.println("</tr>");
            out.println("<tr>");
            out.println("<td>Catalog Request:</td>");
            out.println("<td>");
            if (theCatalogRequest == null) {
                out.println("No");
            } else {
                out.println("Yes");
            }
            out.println("</td>");
            out.println("</tr>");
            out.println("</table>");
            out.println("<div style='border:1px solid #ddd;"
                    + "margin-top:40px;font-size:90%'>");

            out.println("Debug Info<br/>");
            Enumeration<String> parameterNames = request.getParameterNames();
            while (parameterNames.hasMoreElements()) {
                String paramName = parameterNames.nextElement();
                out.println(paramName + ": ");
                String[] paramValues = request.getParameterValues(paramName);
                for (String paramValue : paramValues) {
                    out.println(paramValue + "<br/>");
                }
            }
            out.println("</div>");
            out.println("</body>");
            out.println("</html>");
        }
    }
}
