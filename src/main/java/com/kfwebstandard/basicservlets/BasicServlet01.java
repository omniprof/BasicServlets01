package com.kfwebstandard.basicservlets;

import java.io.IOException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Servlet implementation class BasicServlet01
 */
@WebServlet(description = "First servlet example", urlPatterns = {"/BasicServlet01"})
public class BasicServlet01 extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private final static Logger LOG = LoggerFactory.getLogger(BasicServlet01.class);

    /**
     * Constructor
     */
    public BasicServlet01() {
        super();
        // Can only be used to initialize class variables
        // Class variables are not thread safe so should rarely be used
        // Cannot communicate with the servlet container
        // Generally not implemented
    }

    /**
     * The servlet is initialized by calling the init () method.
     *
     * @param config
     * @throws javax.servlet.ServletException
     */
    @Override
    public void init(ServletConfig config) throws ServletException {
        // Called once the servlet is constructed
        // Can interact with the servlet container
        // Can initialize anything that is available to every
        // thread of this servlet
        LOG.debug("init");
    }

    /**
     * Destructor
     */
    @Override
    public void destroy() {
        // Java does not have the automatic destructor of C++
        // This method is called by the server just before the
        // servlet is about to be removed from memory
        LOG.debug("destroy");
    }

    /**
     * getter for ServletConfig object
     *
     * @return
     */
    @Override
    public ServletConfig getServletConfig() {
        // ServletConfig object is created by the server for each servlet
        // Contains initialization and startup parameters for this servlet
        LOG.debug("getServletConfig");
        return super.getServletConfig();
    }

    /**
     * getter for a String of information
     *
     * @return
     */
    @Override
    public String getServletInfo() {
        // Returns information about the servlet, such as author, version, 
        // and copyright.
        LOG.debug("getServletInfo");
        return "BasicServlet01 Version 2.0";
    }

    /**
     * Master method for all requests to this servlet
     *
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Called by the servlet container to allow the servlet to respond to a request
        // All it does in the super class is figure out which 'do' method is required
        // and proceeds to call it. Rarely overridden.
        LOG.debug("service");
    }

    /**
     * Called by the server (via the service method) to allow a servlet to
     * handle a GET request. CRUD -> R
     *
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Retrieves a resource from the server
        // Idempotent and safe 
        // For example, most form queries have no side effects. 
        // If a client request is intended to change stored data, 
        // the request should use some other HTTP method.
        // The contents of the query string is part of the address field so bookmarks
        // include the query string
        // Maximum length between 2K and 8K depending on server and browser
        LOG.debug("doGet");
    }

    /**
     * Called by the server (via the service method) to allow a servlet to
     * handle a POST request. CRUD -> C
     *
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Adds a resource to a server
        // Not safe or idempotent. 
        // Operations requested through POST can have side effects
        // Query string containing data is in a different location in the http request so
        // bookmarks do not include the query string.
        // No limit on the length
        LOG.debug("doPost");
    }

    /**
     * Called by the server (via the service method) to allow a servlet to
     * handle a PUT request. CRUD -> U
     *
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Updates an existing resource on the server
        // Idempotent but not safe
        // For web applications this method is rarely used 
        LOG.debug("doPut");
    }

    /**
     * Called by the server (via the service method) to allow a servlet to
     * handle a DELETE request. CRUD -> D
     *
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Deletes a resource on the server
        // Idempotent but not safe
        // For web applications this method is rarely used
        LOG.debug("doDelete");
    }

}
