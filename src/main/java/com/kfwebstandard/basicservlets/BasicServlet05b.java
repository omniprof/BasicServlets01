package com.kfwebstandard.basicservlets;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Ken
 */
@WebServlet(urlPatterns = {"/SecondServlet"})
public class BasicServlet05b extends HttpServlet {

    private final static Logger LOG = LoggerFactory.getLogger(BasicServlet05b.class);

    private static final String TITLE = "Demo of Forward and Redirect 02";

    /**
     * A forward goes to the same method if came from. A forward in a POST goes
     * to a POST. A forward in a GET goes to a GET.
     *
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        // Attributes are objects so we must cast to their actual type
        String theName = (String) request.getAttribute("name");

        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            // Now for some ugly code
            out.println("<html>");
            out.println("<head>");
            out.println("<title>" + TITLE + "</title>");
            out.println("<link rel='stylesheet' href='/BasicServlets01/styles/main.css' type='text/css'/>");
            out.println("</head>");
            out.println("<body><h1>" + TITLE + "  POST</h1>");
            out.println("<h2>Hello " + theName + "</h2></br>");
            out.println("<h1>The name should appear after the Hello if we were forwarded</h1></br>");
            out.println("<h1>There should be an evil null after the Hello if we were redirected</h1></br>");

            out.println("</body>");
            out.println("</html>");
        }
    }

    /**
     * A redirect always goes to a GET
     *
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        // Attributes are objects so we must cast to their actual type
        String theName = (String) request.getAttribute("name");

        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            // Now for some ugly code
            out.println("<html>");
            out.println("<head>");
            out.println("<title>" + TITLE + "</title></head>");
            out.println("<body><h1>" + TITLE + "  GET</h1>");
            out.println("<h2>Hello " + theName + "</h2></br>");
            out.println("<h1>The name should appear after the Hello if we were forwarded</h1></br>");
            out.println("<h1>There should be an evil null after the Hello if we were redirected</h1></br>");

            out.println("</body>");
            out.println("</html>");
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "BasicServlet05b";
    }// </editor-fold>

}
